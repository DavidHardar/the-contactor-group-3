import React from 'react';
import AppContainer from './src/routes';
import reducers from './src/reducers';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

export default function App() {
	return (
		<Provider store={createStore(reducers)}>
			<AppContainer />

		</Provider>
	);
}
