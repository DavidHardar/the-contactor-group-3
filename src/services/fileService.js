import * as FileSystem from 'expo-file-system';
import {downloadAsync, writeAsStringAsync} from 'expo-file-system';
import {BASIC_IMAGE_FILE} from '../constants';
import * as createIdService from './createIdService';

const contactDirectory = `${FileSystem.documentDirectory}contacts`;
const imageDirectory = `${FileSystem.documentDirectory}images`;

const onException = (cb, errorHandler) => {
	try {
		return cb();
	} catch (err) {
		if (errorHandler) {
			return errorHandler(err);
		}

		console.error(err);
	}
};

export const cleanImageDirectory = async () => {
	await FileSystem.deleteAsync(imageDirectory);
};

export const cleanContactDirectory = async () => {
	await FileSystem.deleteAsync(contactDirectory);
};

export const getBasicImage = async image => {
	// Check if directory exists
	const dir = await FileSystem.getInfoAsync(imageDirectory);
	if (!dir.exists) {
		await setupImageDirectory();

		await onException(() => downloadAsync(image, `${imageDirectory}/${BASIC_IMAGE_FILE}`));
	}

	return `${imageDirectory}/${BASIC_IMAGE_FILE}`;
};

export const copyFile = async (file, newLocation) => await onException(() => FileSystem.copyAsync({
	from: file,
	to: newLocation,
}));

export const deleteImage = async fileName => {
	await FileSystem.deleteAsync(fileName);
};

export const addImage = async imageLocation => {
	const folderSplit = imageLocation.split('/');
	const fileName = folderSplit[folderSplit.length - 1];
	await onException(() => copyFile(imageLocation, `${imageDirectory}/${fileName}`));

	return `${imageDirectory}/${fileName}`;
};

export const addNativeImage = async imageLocation => {
	const fileName = createIdService.makeid();
	await onException(() => copyFile(imageLocation, `${imageDirectory}/${fileName}`));

	return `${imageDirectory}/${fileName}`;
};

export const deleteContact = async fileName => {
	await FileSystem.deleteAsync(`${contactDirectory}/${fileName}`);
};

export const addContact = async contactInfo => {
	if (contactInfo.editContact) {
		delete contactInfo.editContact;
	}

	if (!contactInfo.nativeId) {
		contactInfo.nateiveId = '-1';
	}

	await onException(() => writeAsStringAsync(`${contactDirectory}/${contactInfo.fileName}`, JSON.stringify(contactInfo)));
};

export const remove = async name => await onException(() => FileSystem.deleteAsync(`${imageDirectory}/${name}`, {idempotent: true}));

export const loadImage = async fileName => await onException(() => FileSystem.readAsStringAsync(`${imageDirectory}/${fileName}`, {
	encoding: FileSystem.EncodingType.Base64,
}));

const setupImageDirectory = async () => {
	const dir = await FileSystem.getInfoAsync(imageDirectory);
	if (!dir.exists) {
		await FileSystem.makeDirectoryAsync(imageDirectory);
	}
};

export const getAllImages = async () => {
	// Check if directory exists
	await setupImageDirectory();

	const result = await onException(() => FileSystem.readDirectoryAsync(imageDirectory));
	return Promise.all(result.map(async fileName => ({
		name: fileName,
		type: 'image',
		file: await loadImage(fileName),
	})));
};

export const loadContact = async fileName => await onException(() => FileSystem.readAsStringAsync(`${contactDirectory}/${fileName}`));

const setupContactDirectory = async () => {
	const dir = await FileSystem.getInfoAsync(contactDirectory);
	if (!dir.exists) {
		await FileSystem.makeDirectoryAsync(contactDirectory);
	}
};

export const getAllContacts = async () => {
	// Check if directory exists
	await setupContactDirectory();

	const result = await onException(() => FileSystem.readDirectoryAsync(contactDirectory));
	return Promise.all(result.map(async fileName => await loadContact(fileName),
	));
};
