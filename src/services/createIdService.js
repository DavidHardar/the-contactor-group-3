export const makeid = () => {
	const length = 32;
	let id = '';
	const characters = 'abcdef0123456789';
	const charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
		id += characters.charAt(Math.floor(Math.random()
 * charactersLength));
	}

	return id;
};
