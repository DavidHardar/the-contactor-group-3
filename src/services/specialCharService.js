export const specialChar = stringToReplace => {
	let result = stringToReplace.replace(/[^\w\s]/g, '-');
	result = result.replace(/\s/g, '_');
	return result;
};
