import * as Contacts from 'expo-contacts';
import * as createIdService from './createIdService';
import * as specialCharService from './specialCharService';
import * as fileService from './fileService';
import {BASIC_IMAGE} from '../constants';

export const getContacts = async () => {
	const {status} = await Contacts.requestPermissionsAsync();
	if (status === 'granted') {
		const {data} = await Contacts.getContactsAsync();

		if (data.length > 0) {
			const photo = await fileService.getBasicImage(BASIC_IMAGE);
			const filteredData = data.filter(contact => contact.phoneNumbers !== undefined);

			const useableData = filteredData.map(async contact => {
				const id = createIdService.makeid();
				const filename = specialCharService.specialChar(contact.name).concat('-', id).concat('.json');
				let contactPhoto = photo;
				if (contact.imageAvailable) {
					contactPhoto = await fileService.addNativeImage(contact.image.uri);
				}

				return {
					id,
					name: contact.name,
					phoneNumber: contact.phoneNumbers[0].number,
					fileName: filename,
					nativeId: contact.id,
					photo: contactPhoto,
				};
			});
			return Promise.all(useableData);
		}
	}
};
