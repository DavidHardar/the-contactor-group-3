// Contact constants
export const ADD_CONTACT = 'ADD_CONTACT';
export const ADD_CONTACTS = 'ADD_CONTACTS';
export const EDIT_CONTACT = 'EDIT_CONTACT';
export const REMOVE_CONTACT = 'REMOVE_CONTACT';

// Search bar constants
export const UPDATE_SEARCH_BAR = 'UPDATE_SEARCH_BAR';

// Visible contact constants
export const UPDATE_VISIBLE_CONTACTS = 'UPDATE_VISIBLE_CONTACTS';

export const BASIC_IMAGE = 'https://www.stignatius.co.uk/wp-content/uploads/2020/10/default-user-icon.jpg';
export const BASIC_IMAGE_FILE = 'basicImage.jpg';
