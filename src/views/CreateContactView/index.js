import React from 'react';
import {View} from 'react-native';
import ContactForm from '../../components/ContactForm';
import PropTypes from 'prop-types';
import styles from './styles';

const CreateContactView = ({navigation: {goBack}}) => (
	<View style={styles.container}>
		<ContactForm goBack={ goBack }/>
	</View>
);

CreateContactView.propTypes = {
	navigation: PropTypes.shape({
		goBack: PropTypes.func.isRequired,
	}).isRequired,
};

export default CreateContactView;

