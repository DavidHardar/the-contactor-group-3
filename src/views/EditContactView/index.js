import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles';
import {View} from 'react-native';
import EditContactForm from '../../components/EditContactForm';

const EditContactView = ({route, navigation: {goBack}}) => (
	<View style={styles.container}>
		<EditContactForm
			id={route.params.id}
			goBack={ goBack }/>
	</View>
);

EditContactView.propTypes = {
	route: PropTypes.shape({
		params: PropTypes.shape({
			id: PropTypes.string.isRequired,
		}).isRequired,
	}).isRequired,
	navigation: PropTypes.shape({
		goBack: PropTypes.func.isRequired,
	}).isRequired,
};

export default EditContactView;

