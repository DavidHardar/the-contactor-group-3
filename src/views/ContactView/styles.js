import {StyleSheet} from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'transparent',
		alignItems: 'center',
		paddingHorizontal: '2%',
	},
	image: {
		flexBasis: '25%',
		width: 150,
		height: 150,
		resizeMode: 'contain',
	},
	linearGradient: {
		flex: 1,
		paddingLeft: 15,
		paddingRight: 15,
		borderRadius: 5,
	},
});
