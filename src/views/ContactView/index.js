import React, {useEffect} from 'react';
import {View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import ContactList from '../../components/ContactList';
import Toolbar from '../../components/Toolbar';
import addContacts from '../../actions/addContactsAction';
import updateVisibleContacts from '../../actions/updateVisibleContactsAction';
import * as fileService from '../../services/fileService';
import * as contactsService from '../../services/contactsService';
import updateVisibleContracts from '../../actions/updateVisibleContactsAction';
import styles from './styles';

const ContactView = () => {
	// Use these two fileService calls to remove all contact and photo data
	// fileService.cleanContactDirectory();
	// fileService.cleanImageDirectory();
	const contacts = useSelector(state => state.contacts);
	const visualContacts = useSelector(state => state.visualContacts);
	const searchBar = useSelector(state => state.searchBar);
	const dispatch = useDispatch();

	useEffect(() => {
		(async () => {
			const oldContactsJson = await fileService.getAllContacts();
			const oldContacts = oldContactsJson.map(contact => (JSON.parse(contact)));
			const nativeContacts = await contactsService.getContacts();

			if (nativeContacts) {
				const newNativeContacts = nativeContacts.filter(nativeContact =>
					!oldContacts.some(oldContact => oldContact.nativeId === nativeContact.nativeId));
				newNativeContacts.forEach(contact => {
					oldContacts.push(contact);
					fileService.addContact(contact);
				});
			}

			oldContacts.sort(compareContacts);
			dispatch(addContacts(oldContacts));
			dispatch(updateVisibleContacts(oldContacts));
		})();
	}, []);

	useEffect(() => {
		dispatch(updateVisibleContracts(contacts));
	}, [contacts]);

	useEffect(() => {
		const newVisualContacts = contacts.filter(contact => matchNameWithSearch(contact.name, searchBar));
		dispatch(updateVisibleContacts(newVisualContacts));
	}, [searchBar]);

	const compareContacts = (a, b) => {
		if (a.name < b.name) {
			return -1;
		}

		if (a.name > b.namee) {
			return 1;
		}

		return 0;
	};

	const matchNameWithSearch = (name, search) => {
		const compName = name.toLowerCase();
		const compSearch = search.toLowerCase();
		return compName.includes(compSearch);
	};

	return (

		<View style={styles.container}>
			<Toolbar />
			<ContactList contacts={ visualContacts }/>
		</View>

	);
};

export default ContactView;

