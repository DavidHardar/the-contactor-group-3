import {StyleSheet} from 'react-native';
import {GREY} from "../../styles/colors";

export default StyleSheet.create({

	touch: {
		flex: 1,
		alignItems: "center",
		marginTop: "5%"
	},
	container: {
		flex: 1

	},
	image: {
		width: 200,
		height: 200,
		borderRadius: 500,
		marginTop: 10,
		borderWidth: 4,
		borderColor: 'black',
	},
	info: {
		flex: 0.7,
		width: "70%",
		justifyContent: "space-between",
		fontSize: 20,
		backgroundColor: "white",
		borderRadius: 10,
		textAlignVertical: "center",
		textAlign: "center"

	},
	touchable: {
		flex: 0.8,
		flexDirection: "column",
		justifyContent: "space-between",
		textAlignVertical: "center",
		justifyContent: "center",
		alignItems: "center"
	},
	edit: {
		flex: 0.6,
		flexDirection: "row-reverse",
		alignItems: 'flex-end',
		justifyContent:"flex-end",
		alignSelf: "flex-end",
		marginLeft: "4%",
	},
	edittext: {
		flex: 0.2,
		fontSize: 20,
		flexDirection: "row-reverse",
		alignItems: 'center',
		alignSelf: "flex-end",
		marginLeft: "8.5%",
	},
	imageview:{
		flex: 2,
		alignItems: "center"
	},
	callbutton:{
		alignItems: "center",
		justifyContent: "center",
		height: 80,
		width: 80,
		borderRadius: 40,
		backgroundColor: "green"
	},
	editbutton: {
		alignItems: "center",
		borderRadius: 1000,
		backgroundColor: GREY,
		height: 60,
		width: 60
	}
});
