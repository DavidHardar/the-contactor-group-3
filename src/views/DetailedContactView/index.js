import React from 'react';
import styles from './styles';
// import {FontAwesome} from '@expo/vector-icons';
import {Text, View, TouchableHighlight, Image, Linking} from 'react-native';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import PropTypes from 'prop-types';
import {Ionicons, MaterialIcons  } from '@expo/vector-icons';

const DetailedContactView = ({route}) => {
	const contact = useSelector(state =>
		state.contacts.find(contact =>
			contact.id === route.params.id),
	);

	const {navigate} = useNavigation();
	return (
		<View style={styles.container}>
			<View style={styles.edit}>
			<TouchableHighlight style={styles.editbutton}
				onPress={() => navigate('Edit a Contact', {
					id: contact.id,
				})}>
					<MaterialIcons name="edit" size={50} color="black" />
			</TouchableHighlight>
			</View>
			<View style = {styles.edittext}> 
				<Text>Edit</Text>
			</View>
			<View style={styles.imageview}>
				<Image source= {{uri: contact.photo}} style={styles.image}/>
			</View>
			<View style = {styles.touchable}>
				<Text style = {styles.info}>{contact.name} </Text>
			</View>

			<View style = {styles.touchable}>
				<Text style = {styles.info}>{contact.phoneNumber} </Text>
			</View>
			<View  style = {styles.touch}>
			<TouchableHighlight style={styles.callbutton}>
				<Ionicons  onPress={() => {
					Linking.openURL(`tel:${contact.phoneNumber}`);
				}} name="call" size={50} color="white" />
			</TouchableHighlight>
			</View>

		</View>
	);
};

DetailedContactView.propTypes = {
	route: PropTypes.shape({
		params: PropTypes.shape({
			id: PropTypes.string.isRequired,
			name: PropTypes.string.isRequired,
			photo: PropTypes.string.isRequired,
			phoneNumber: PropTypes.string.isRequired,
			fileName: PropTypes.string.isRequired,
		}).isRequired,
	}).isRequired,
};

export default DetailedContactView;

