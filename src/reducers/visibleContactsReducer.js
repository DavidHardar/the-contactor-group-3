import * as constants from '../constants';

export default function (state = [], action) {
	switch (action.type) {
		case constants.UPDATE_VISIBLE_CONTACTS: return action.payload;
		default: return state;
	}
}
