import {combineReducers} from 'redux';
import searchBar from './searchBarReducer';
import contacts from './contactsReducer';
import visualContacts from './visibleContactsReducer';

export default combineReducers({
	searchBar,
	contacts,
	visualContacts,
});
