import * as constants from '../constants';

export default function (state = '', action) {
	switch (action.type) {
		case constants.UPDATE_SEARCH_BAR: return action.payload;
		default: return state;
	}
}
