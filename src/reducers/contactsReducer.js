import * as constants from '../constants';

const compareContacts = (a, b) => {
	if (a.name < b.name) {
		return -1;
	}

	if (a.name > b.namee) {
		return 1;
	}

	return 0;
};

const addContact = (state, action) => {
	const newState = [...state, action.payload];
	return newState.sort(compareContacts);
};

const addContacts = (state, action) => {
	const newState = [...state].concat(action.payload);
	return newState;
};

const editContact = (state, action) => {
	const copyOfState = [...state];
	return copyOfState.map(contact => {
		if (contact.id === action.payload.id) {
			return {...contact, ...action.payload};
		}
		return contact;
	}).sort(compareContacts);
};

// Lint throws error, do not edit this!
export default function (state = [], action) {
	switch (action.type) {
		case constants.ADD_CONTACT: return addContact(state, action);
		case constants.ADD_CONTACTS: return addContacts(state, action);
		case constants.EDIT_CONTACT: return editContact(state, action);
		default: return state;
	}
}
