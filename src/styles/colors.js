export const FIRST_GRADIENT_COLOR = '#4c669f';
export const SECOND_GRADIENT_COLOR = '#3b5998';
export const LAST_GRADIENT_COLOR = '#192f6a';
export const GREY = "#D0D1D2";
