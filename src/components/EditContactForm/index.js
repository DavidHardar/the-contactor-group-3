import React, {useEffect, useCallback} from 'react';
import {View, TextInput, Text, TouchableHighlight} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useForm, Controller} from 'react-hook-form';
import styles from './styles';
import PropTypes from 'prop-types';
import editContact from '../../actions/editContactAction';
import * as charService from '../../services/specialCharService';
import * as fileService from '../../services/fileService';
import * as imageService from '../../services/imageService';
import {BASIC_IMAGE_FILE} from '../../constants';
import {MaterialIcons} from '@expo/vector-icons';

const EditContactForm = ({id, goBack}) => {
	let photo = "";
	const contact = useSelector(state =>
		state.contacts.find(contact =>
			contact.id === id),
	);
	const { control, handleSubmit, formState: { errors } } = useForm({
		defaultValues: {
			name: contact.name,
			phoneNumber: contact.phoneNumber
		}
	});
	const dispatch = useDispatch();
	

	const takePhoto = async () => {
		photo = await imageService.takePhoto();
	};

	const selectFromCameraRoll = async () => {
		photo = await imageService.selectFromCameraRoll();
	};

	const onSubmit = async data => {
		if (photo === '') {
			data.photo = contact.photo;
		}
		else{
			const imageSplit = contact.photo.split('/');
			const imageName = imageSplit[imageSplit.length - 1];
			if(imageName !== BASIC_IMAGE_FILE){
				fileService.deleteImage(contact.photo);
			}
			data.photo = await fileService.addImage(photo);
		}

		// Create id
		data.id = id;
		// Create a filename
		const tempName = charService.specialChar(data.name);
		const thisfileName = tempName.concat('-', data.id);
		const newfileName = thisfileName.concat('.json');
		data.fileName = newfileName;

		// Update state in views
		dispatch(editContact(data));
		// Create new file and delete new one in file system
		fileService.deleteContact(contact.fileName);
		fileService.addContact(data);
		goBack();
	}

	return (
		<View style={styles.view}>
			<View style={styles.innerview}>
				<Text style={styles.text}>Name</Text>
					<Controller
						control={control}
						rules={{
							required: true,
						}}
						render={({ field: { onChange, onBlur, value } }) => (
						<TextInput
							style={[styles.textinput, errors.name && styles.inputerror]}
							onBlur={onBlur}
							onChangeText={onChange}
							value={value}
						/>
						)}
						name="name"
					/>
					{errors.name && <Text>This is required.</Text>}
			</View>
			<View style={styles.innerview}>
				<Text style={styles.text}>Phone number</Text>
				<Controller
					control={control}
					rules={{
						required: true,
						minLength: 3,
						maxLength: 15,
						pattern: /^[0-9\s\-\+]*$/g
					}}
					render={({ field: { onChange, onBlur, value } }) => (
					<TextInput
						style={[styles.textinput, errors.phoneNumber && styles.inputerror]}
						onBlur={onBlur}
						maxLength={15}
						onChangeText={onChange}
						value={value}
					/>
					)}
					name="phoneNumber"
				/>
				{errors.phoneNumber && errors.phoneNumber.type === "required" && <Text>Phonenumber is required</Text>}
				{errors.phoneNumber && errors.phoneNumber.type === "minLength" && <Text>Must be bigger than 3</Text>}
				{errors.phoneNumber && errors.phoneNumber.type === "maxLength" && <Text>Must be smaller than 16</Text>}
				{errors.phoneNumber && errors.phoneNumber.type === "pattern" && <Text>Only numbers and -, + and spaces allowed</Text>}
			</View>
				<View style={styles.cameras}>
			<TouchableHighlight style={styles.add} onPress={() => takePhoto()}>
				<MaterialIcons name="camera-alt" size={36} color="black" />
				</TouchableHighlight>
				<TouchableHighlight style={styles.add} onPress={() => selectFromCameraRoll()}>
					<MaterialIcons name="photo-library" size={36} color="black" />
				</TouchableHighlight>
			</View>
			<View style={styles.innerview}>
				<TouchableHighlight style={styles.button} onPress={handleSubmit(onSubmit)}>
					<Text style= {styles.buttontext}>Submit</Text>
				</TouchableHighlight>
			</View>
		</View>
	);
};

EditContactForm.propTypes = {
	goBack: PropTypes.func.isRequired,
	id: PropTypes.string.isRequired,
};

export default EditContactForm;
