import React from 'react';
import {View, TextInput, Text, TouchableHighlight} from 'react-native';
import {useForm, Controller} from 'react-hook-form';
import styles from './styles';
import PropTypes from 'prop-types';
import addContact from '../../actions/addContactAction';
import * as fileService from '../../services/fileService';
import * as idService from '../../services/createIdService';
import * as imageService from '../../services/imageService';
import * as charService from '../../services/specialCharService';
import {BASIC_IMAGE} from '../../constants';
import {useDispatch} from 'react-redux';
import {MaterialIcons} from '@expo/vector-icons';

const ContactForm = ({goBack}) => {
	let photo = "";
	const { control, handleSubmit, formState: { errors } } = useForm({
		defaultValues: {
		  name: '',
		  phoneNumber: ''
		}
	  });
	  
	const disPatch = useDispatch();

	const takePhoto = async () => {
        photo = await imageService.takePhoto();
    }

	const selectFromCameraRoll = async () => {
		photo = await imageService.selectFromCameraRoll();
    }

	const onSubmit = async data => {
		// Get a basic photo
		let baseImg = await fileService.getBasicImage(BASIC_IMAGE);
		// Set either custom or basic photo
		if (photo === undefined || photo === "") {
			data.photo = baseImg;
		}
		else{
			data.photo = await fileService.addImage(photo);
		}
		// Create id
		data.id = idService.makeid();
		// Create a filename
		const tempName = charService.specialChar(data.name);
		const fileName = tempName.concat('-', data.id);
		const newfileName = fileName.concat('.json');
		data.fileName = newfileName;

		disPatch(addContact(data));
		fileService.addContact(data);
		goBack();
	};

	return (
		<View style={styles.view}>
			<View style={styles.innerview}>
				<Text style={styles.text}>Name</Text>
				<Controller
					control={control}
					rules={{
						required: true,
					}}
					render={({ field: { onChange, onBlur, value } }) => (
					<TextInput
						style={[styles.textinput, errors.name && styles.inputerror]}
						onBlur={onBlur}
						onChangeText={onChange}
						value={value}
						placeholder={"Name"}
					/>
					)}
					name="name"
				/>
				{errors.name && <Text style= {styles.errorText}>This is required.</Text>}
			</View>

			<View style={styles.innerview}>
				<Text style={styles.text}>Phone number</Text>
				<Controller
					control={control}
					rules={{
						required: true,
						minLength: 3,
						maxLength: 15,
						pattern: /^[0-9\s\-\+]*$/g
					}}
					render={({ field: { onChange, onBlur, value } }) => (
					<TextInput
						keyboardType={"numeric"}
						style={[styles.textinput, errors.phoneNumber && styles.inputerror]}
						onBlur={onBlur}
						maxLength={15}
						onChangeText={onChange}
						value={value}
						placeholder={"Phone number"}
					/>
					)}
					name="phoneNumber"
				/>
				{errors.phoneNumber && errors.phoneNumber.type === "required" && <Text style= {styles.errorText}>Phonenumber is required</Text>}
				{errors.phoneNumber && errors.phoneNumber.type === "minLength" && <Text style= {styles.errorText}>Must be bigger than 3</Text>}
				{errors.phoneNumber && errors.phoneNumber.type === "maxLength" && <Text style= {styles.errorText}>Must be smaller than 16</Text>}
				{errors.phoneNumber && errors.phoneNumber.type === "pattern" && <Text style= {styles.errorText}>Only numbers and -, + and spaces allowed</Text>}
			</View>
			<View style={styles.cameras}>
				<TouchableHighlight style={styles.add} onPress={() => takePhoto()}>
					<MaterialIcons name="camera-alt" size={36} color="black" />
				</TouchableHighlight>
				<TouchableHighlight style={styles.add} onPress={() => selectFromCameraRoll()}>
					<MaterialIcons name="photo-library" size={36} color="black" />
				</TouchableHighlight>
			</View>
			<View style={styles.innerview}>
				<TouchableHighlight style={styles.button} onPress={handleSubmit(onSubmit)}>
					<Text style= {styles.buttontext}>Submit</Text>
				</TouchableHighlight>
			</View>
		</View>
	);
};

ContactForm.propTypes = {
	goBack: PropTypes.func.isRequired,
};

export default ContactForm;
