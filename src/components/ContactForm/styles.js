import {StyleSheet} from 'react-native';

export default StyleSheet.create({
	view: {
		flex: 1,
		width: '100%',
		justifyContent: 'space-around',
		alignItems: 'center',
	},

	innerview: {
		flex: 1,
		width: '100%',
		padding: '10%',
		justifyContent: 'center',
		alignItems: 'center',
	},

	textinput: {
		width: '100%',
		borderWidth: 3,
		borderColor: 'black',
		borderRadius: 10,
		textAlign: 'left',
		backgroundColor: 'white',
		paddingLeft: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 8,
		},
		shadowOpacity: 0.46,
		shadowRadius: 11.14,

		elevation: 17,
	},
	inputerror: {
		borderColor: '#e02727',
		shadowColor: "#e02727",
	},

	text: {
		marginBottom: 5,
		fontSize: 20,
	},
	errorText: {
		fontSize: 16,
		borderRadius: 5,
		alignSelf: 'flex-end',
		textDecorationColor: "red",
		color: '#e02727',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 5,
		},
		shadowOpacity: 0.34,
		shadowRadius: 6.27,

		elevation: 10,
	},

	button: {
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: 12,
		paddingHorizontal: 32,
		borderRadius: 4,
		elevation: 3,
		backgroundColor: 'green',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 8,
		},
		shadowOpacity: 0.46,
		shadowRadius: 11.14,
	},
	cameras: {
		flex: 1,
		flexDirection: "row",
		justifyContent: 'center',
	},
	add: {
		alignItems: 'center',
		justifyContent: 'space-around',
		marginTop: 20,
		marginLeft: 20,
		marginRight: 20,
		height: 60,
		width: 60,
		borderRadius: 30,
		elevation: 3,
		backgroundColor: 'white',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 8,
		},
		shadowOpacity: 0.46,
		shadowRadius: 11.14,
	},
	

	buttontext: {
		fontSize: 20,
	},

});
