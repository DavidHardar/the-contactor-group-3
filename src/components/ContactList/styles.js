import {StyleSheet} from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'transparent',
		alignItems: 'center',
		justifyContent: 'center',
	},
	image: {
		flexBasis: '25%',
		width: 150,
		height: 150,
		resizeMode: 'contain',
	},
});
