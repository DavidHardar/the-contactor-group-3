import React from 'react';
import {View, FlatList} from 'react-native';
import PropTypes from 'prop-types';
import Contact from '../Contact';
import styles from './styles';

const ContactList = ({contacts}) => (
	<View style={ styles.container }>
		<FlatList
			data={contacts}
			renderItem={({item}) => (
				<Contact
					name={item.name}
					fileName={item.fileName}
					phoneNumber={item.phoneNumber}
					photo={item.photo}
					id={item.id}
				/>
			)}
			keyExtractor={contacts => contacts.id} />
	</View>
);

ContactList.propTypes = {
	contacts: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		photo: PropTypes.string.isRequired,
		phoneNumber: PropTypes.string.isRequired,
	})).isRequired,
};

export default ContactList;

