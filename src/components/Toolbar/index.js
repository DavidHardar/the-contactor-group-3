import React from 'react';
import {useDispatch} from 'react-redux';
import {View, TouchableHighlight, TextInput} from 'react-native';
import styles from './styles';
import {AntDesign} from '@expo/vector-icons';
import {Entypo} from '@expo/vector-icons';
import {useNavigation} from '@react-navigation/native';
import updateSearchBar from '../../actions/updateSearchBarAction';

const Toolbar = () => {
	const {navigate} = useNavigation();
	const dispatch = useDispatch();
	return (
		<View style={styles.toolbar}>
			<TextInput
				onChangeText={text => {
					dispatch(updateSearchBar(text));
				}}
				placeholder="Search"
				style={styles.searchbar}
			/>

			<TouchableHighlight style={styles.logo} onPress={() => navigate('Create a Contact')}>
				<AntDesign name="plus" size={24} color="black" />
			</TouchableHighlight>


		</View>
	);
};

export default Toolbar;
