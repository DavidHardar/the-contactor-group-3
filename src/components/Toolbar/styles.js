import {StyleSheet} from 'react-native';
import { GREY } from '../../styles/colors';

export default StyleSheet.create({

	toolbar: {
		justifyContent: 'flex-end',
		flexDirection: 'row',
		height: 70,
		width: '100%',
		backgroundColor: 'transparent',
		alignContent: 'center',
		paddingTop: 20,
		borderBottomWidth: 3,
		borderColor: 'black',

	},
	logo: {
		width: 40,
		height: 40,
		borderRadius: 20,
		marginLeft: 5,
		marginRight: 5,
		resizeMode: 'contain',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: GREY,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 5,
		},
		shadowOpacity: 1,
		shadowRadius: 6.27,

		elevation: 10,
	},
	searchbar: {
		flex: 0.97,
		backgroundColor: 'white',
		padding: 10,
		borderWidth: 3,
		borderColor: 'black',
		borderRadius: 12,
		marginBottom: 5,
		marginTop: 5,

	},
});
