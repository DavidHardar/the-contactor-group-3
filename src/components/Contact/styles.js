import {StyleSheet} from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		width: '95%',
        overflow: 'hidden',
		padding: 10,
	},
	image: {
		
		width: 60,
		height: 60,
		resizeMode: 'contain',
        borderRadius: 400,
	},
	title: {
		fontSize: 15,
		textAlignVertical: "auto"
	},
	titleContainer: {
		flexBasis: '70%',
		justifyContent: "center"

	},

	touchable: {
		flex: 1,
		backgroundColor: '#ededed',
		borderColor: 'black',
		borderWidth: 3,
		marginTop: 15,
		borderRadius: 12,
	},
});
