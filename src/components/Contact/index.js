import React from 'react';
import {View, Text, Image, TouchableHighlight} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import PropTypes from 'prop-types';
import styles from './styles';

const Contact = ({name, photo, id, phoneNumber, fileName}) => {
	const {navigate} = useNavigation();
	return (
		<TouchableHighlight
			style={styles.touchable}
			onPress={() => {
				navigate('Detailed Contact View', {
					id,
					name,
					photo,
					phoneNumber,
					fileName,
				});
			}}
		>
			<View style={ styles.container }>
				<View style={ styles.titleContainer }>
					<Text style={ styles.title }>{ name }</Text>
				</View>
				<Image
					style={ styles.image }
					source={{uri: photo}}/>

			</View>
		</TouchableHighlight>);
};

Contact.propTypes = {
	name: PropTypes.string.isRequired,
	photo: PropTypes.string.isRequired,
	id: PropTypes.string.isRequired,
	phoneNumber: PropTypes.string.isRequired,
	fileName: PropTypes.string.isRequired,
};

export default Contact;
