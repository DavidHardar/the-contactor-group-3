import React from 'react';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import styles from './styles';
import {LinearGradient} from 'expo-linear-gradient';
import {FIRST_GRADIENT_COLOR, SECOND_GRADIENT_COLOR, LAST_GRADIENT_COLOR} from '../styles/colors';

const Stack = createStackNavigator();

/* Import View Routes here */
import ContactView from '../views/ContactView';
import CreateContactView from '../views/CreateContactView';
import DetailedContactView from '../views/DetailedContactView';
import EditContactView from '../views/EditContactView';

export default function Routes() {
	const mainTheme = {
		...DefaultTheme,
		colors: {
			...DefaultTheme.colors,
			background: 'transparent',
		},
	};

	return (
		<LinearGradient colors={[FIRST_GRADIENT_COLOR, SECOND_GRADIENT_COLOR, LAST_GRADIENT_COLOR]} style={styles.linearGradient}>
			<NavigationContainer theme={mainTheme}>
				<Stack.Navigator initialRouteName="Contacts">
					<Stack.Screen name="Contacts" component={ContactView} />
					<Stack.Screen name="Create a Contact" component={CreateContactView} />
					<Stack.Screen name="Edit a Contact" component={EditContactView} />
					<Stack.Screen name="Detailed Contact View" component={DetailedContactView}/>
				</Stack.Navigator>
			</NavigationContainer>
		</LinearGradient>

	);
}
