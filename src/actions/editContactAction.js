import * as constants from '../constants';

/*
    Takes a contact object and sends it to the reducer

    {
        "id": Id of the contact,
        "name": Name of the contact,
        "phoneNr" Phone number of the contact,
        "imagePath" The filepath to the image
    }
*/
const editContact = contact => ({
	type: constants.EDIT_CONTACT,
	payload: contact,
});

export default editContact;
