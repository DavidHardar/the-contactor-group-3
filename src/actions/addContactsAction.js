import * as constants from '../constants';

/*
    Takes a list of contact objects and sends it to the reducer

    {
        "id": Id of the contact
        "name": Name of the contact,
        "phoneNr" Phone number of the contact,
        "imagePath" The filepath to the image
    }
*/
const addContacts = contacts => ({
	type: constants.ADD_CONTACTS,
	payload: contacts,
});

export default addContacts;
