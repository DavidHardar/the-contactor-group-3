import * as constants from '../constants';
/*
    Takes a string and sends it to the reducer

*/
const updateSearchBar = inputText => ({
	type: constants.UPDATE_SEARCH_BAR,
	payload: inputText,
});

export default updateSearchBar;
